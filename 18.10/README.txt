This dataset is built from the October 2018 Rode0day bug-finding competition which ran from October 17, 2018 to November 7, 2018.

The archived scoreboard is available at https://rode0day.mit.edu/results/4

The following information is provided:
    download/ contains the data provided to competitors during the competition
    source/[challenge_name]/ contains the source code for all the challenges
    uploads/[user_id]/[challenge_id]/[bug_id] are inputs submitted by users that triggered the given bug_id
    solutions/[challenge_id]/[bug_id] are the inputs we used to validate each bug
    scoreboard.png shows how teams scored points over time
    submissions.csv shows the discovery rate for each bug over time. Only the first time a user triggered each bug is listed
    bugs.csv lists the (bug id, challenge id) pairs of the injected bugs

Other notes:
* The newgrepS target contains bugs injected using the "Apocalypse" bug injection system as described in:
    Roy, Subhajit, et al. "Bug synthesis: challenging bug-finding tools with deep faults." Proceedings of the 2018 26th ACM Joint Meeting on European Software Engineering Conference and Symposium on the Foundations of Software Engineering. ACM, 2018.

* The newgrepS target was manually modified to parse arguments from its input file. These modifications inadvertently introduced a new bug which was discovered by some competitors. It is present in the uploads directory with a bugid of -1.

* The discovery rate for this competition was 96%. 140 unique bugs of the 145 injected bugs were discovered.

* The scores shown on the scoreboard were calculated as follows: ten points for each bugs found plus a bonus point when a team was the first to find a bug

* User IDs can be mapped to a user's profile at https://rode0day.mit.edu/teams

* The format of this archive may change for future months
