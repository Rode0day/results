This dataset is built from the July Rode0day bug-finding competition which ran from July 2, 2018 to June 1, 2018.

The archived scoreboard is available at https://rode0day.mit.edu/results/2

The following information is provided:
    download/ contains the data provided to competitors during the competition
    source/[challenge_name]/ contains the source code for all the challenges
    uploads/[user_id]/[challenge_id]/[bug_id] are inputs submitted by users that triggered the given bug_id
    solutions/[challenge_id]/[challenge_name]-[bug_id] are the inputs we used to validate each bug
    scoreboard.png shows how teams scored points over time
    submissions.csv shows the discovery rate for each bug over time. Only the first time a user triggered each bug is listed
    validated_lava.csv lists the (bug id, challenge id) pairs of lava-validated bugs
    validated_chaff.csv lists the (bug id, challenge id) of competitor-validated chaff bugs

Other notes:
* For this competition, a subset of our injected bugs were, at the time of injection, considered  "potential bugs"; they were modifications to our target programs unaccompanied by crashing inputs. For these bugs, LAVA was unable to generate crashing inputs, but that did not preclude the existence of such inputs. These bugs were injected into our target programs as "chaff bugs", bugs that were likely to be unexplotiable but would look similar our validated, injected bugs.

* Some of these chaff bugs were triggered by competitors (note they had no guarantee of unexploitability). These inputs are present in both the solutions directory as well as the uploads directory. These bugs are listed in validated_chaff.csv.

* The discovery rate for this competition was 87%. 389 unique bugs of the 446 known-triggerable bugs were discovered. Of the known-triggerable bugs, 414 have LAVA-generated solutions and 32 were chaff bugs triggered by competitors.

* The scores shown on the scoreboard were calculated as follows: ten points for each bugs found plus a bonus point when a team was the first to find a bug

* User IDs can be mapped to a user's profile at https://rode0day.mit.edu/teams

* The format of this archive may change for future months
