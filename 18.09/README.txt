This dataset is built from the September Rode0day bug-finding competition which ran from September 21, 2018 to Octoer 17, 2018.

The archived scoreboard is available at https://rode0day.mit.edu/results/3

The following information is provided:
    download/ contains the data provided to competitors during the competition
    source/[challenge_name]/ contains the source code for all the challenges
    uploads/[user_id]/[challenge_id]/[bug_id] are inputs submitted by users that triggered the given bug_id
    solutions/[challenge_id]/[bug_id] are the inputs we used to validate each bug
    scoreboard.png shows how teams scored points over time
    submissions.csv shows the discovery rate for each bug over time. Only the first time a user triggered each bug is listed
    bugs.csv lists the (bug id, challenge id) pairs of the injected bugs

Other notes:
* An uninjected bug was discovered that is present in both jpegS and jpegB, it is present in the uploads directory with a bugid of -1

* The discovery rate for this competition was 100%. 290 unique bugs of the 290 injected bugs were discovered.

* The scores shown on the scoreboard were calculated as follows: ten points for each bugs found plus a bonus point when a team was the first to find a bug

* User IDs can be mapped to a user's profile at https://rode0day.mit.edu/teams

* The format of this archive may change for future months
