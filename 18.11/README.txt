This dataset is built from the November 2018 Rode0day bug-finding competition which ran from November 14, 2018 to December 12, 2018.

The archived scoreboard is available at https://rode0day.mit.edu/results/5

The following information is provided:
    download/ contains the data provided to competitors during the competition
    source/[challenge_name]/ contains the source code for all the challenges
    uploads/[user_id]/[challenge_id]/[bug_id] are inputs submitted by users that triggered the given bug_id
    solutions/[challenge_id]/[bug_id] are the inputs we used to validate each bug
    scoreboard.png shows how teams scored points over time
    submissions.csv shows the discovery rate for each bug over time. Only the first time a user triggered each bug is listed
    bugs.csv lists the (bug id, challenge id) pairs of the injected bugs

Other notes:
* In this competition's File targets, a few unintended bugs were added when uninitialized pointers may be siphoned into the data_flow array. These are indicated by particularly high values for bugids that don't show up anywhere in the target source.  

* The discovery rate for this competition was 66%. 112 unique bugs of the 168 injected bugs were discovered.

* The scores shown on the scoreboard were calculated as follows: ten points for each bugs found plus a bonus point when a team was the first to find a bug

* User IDs can be mapped to a user's profile at https://rode0day.mit.edu/teams

* The format of this archive may change for future months
