This dataset is built from the January 2019 Rode0day bug-finding competition which ran from January 9, 2019 to February 6, 2019.

The archived scoreboard is available at https://rode0day.mit.edu/results/6

The following information is provided:
    download/ contains the data provided to competitors during the competition
    source/[challenge_name]/ contains the source code for all the challenges
    uploads/[user_id]/[challenge_id]/[bug_id] are inputs submitted by users that triggered the given bug_id
    solutions/[challenge_id]/[bug_id] are the inputs we used to validate each bug
    scoreboard.png shows how teams scored points over time
    submissions.csv shows the discovery rate for each bug over time. Only the first time a user triggered each bug is listed
    bugs.csv lists the (bug id, challenge id) pairs of the injected bugs

Other notes:
* The two versions of file each had the same 3 non-injected bugs where are labeled with IDs -1, -2, and -3

* The discovery rate for this competition was 49%. 148 unique bugs of the 297 injected bugs were discovered.

* The scores shown on the scoreboard were calculated as follows: ten points for each bugs found plus a bonus point when a team was the first to find a bug

* User IDs can be mapped to a user's profile at https://rode0day.mit.edu/teams

* The format of this archive may change for future months
